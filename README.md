# Layout bottom buttons move with soft keyboard #

This is an Android example for relative layout with a scrollable middle section and buttons attached to the bottom of the screen. The buttons move up when a soft keyboard is present.

## Features ##

This example includes:

* Relative layout with 2 buttons at the bottom of the screen 
* Support action bar (at the top of the screen)
* Scrollview for content
* Snackbar when pressing buttons

It was made to show how to organize your layout when using the above elements 
pushing the buttons up when the soft keyboard is shown.


## Buttons with same width hack ##
Note that the buttons use a hack for relative positioning the two buttons evenly at the button.
By using a <View> element with width of 0 dp at the center of it's parent we can position the buttons
to the left/right of this <View> and 'stretch' the buttons to the parent start/end.

The normal thing to do in this case was to use a <LinearLayout> inside this <RelativeLayout> and
position the buttons by using layout_width="0dp" and layout_weight="0.5".